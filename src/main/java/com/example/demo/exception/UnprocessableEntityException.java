package com.example.demo.exception;

import com.example.demo.model.Transaction;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNPROCESSABLE_ENTITY, reason="Unprocessable Entity")
public class UnprocessableEntityException extends Exception {

    private Transaction transaction = new Transaction();

    public UnprocessableEntityException(String msg) {
        super(msg);
    }

    public UnprocessableEntityException() {

    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
