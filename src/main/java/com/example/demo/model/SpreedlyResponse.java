package com.example.demo.model;

import java.util.Collections;
import java.util.List;

public class SpreedlyResponse {

    List<SpreedlyError> errors = Collections.emptyList();
    private Transaction transaction = new Transaction();

    public SpreedlyResponse() {
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public List<SpreedlyError> getErrors() {
        return errors;
    }

    public void setErrors(List<SpreedlyError> errors) {
        this.errors = errors;
    }
}
