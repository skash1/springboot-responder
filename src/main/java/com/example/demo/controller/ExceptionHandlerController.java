package com.example.demo.controller;

import com.example.demo.exception.UnprocessableEntityException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
public class ExceptionHandlerController {

    @ExceptionHandler(UnprocessableEntityException.class)
    public ModelAndView handle(HttpServletRequest req, Exception e) {
        UnprocessableEntityException exception = (UnprocessableEntityException) e;
        ModelAndView modelAndView = new ModelAndView();
        if (exception.getTransaction() != null) {
            modelAndView.addObject("transaction", exception.getTransaction());
        }
        modelAndView.addObject("timestamp", new Date());
        modelAndView.addObject("status", HttpStatus.UNPROCESSABLE_ENTITY);
        modelAndView.addObject("message", "");
        modelAndView.addObject("error", "Unprocessable Entity");
        modelAndView.addObject("path", req.getRequestURL());
        return modelAndView;
    }

}
